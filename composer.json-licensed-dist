{
  "name":              "dreamfactory/dreamfactory",
  "description":       "The DreamFactory(tm) Platform.",
  "keywords":          [
    "api",
    "dreamfactory",
    "laravel",
    "rest"
  ],
  "homepage":          "https://www.dreamfactory.com/",
  "license":           "Apache-2.0",
  "type":              "project",
  "authors":           [
    {
      "name":  "Arif Islam",
      "email": "arifislam@dreamfactory.com"
    },
    {
      "name":  "Charles Harmon",
      "email": "charlesharmon@dreamfactory.com"
    },
    {
      "name":  "Lee Hicks",
      "email": "leehicks@dreamfactory.com"
    }
  ],
  "support":           {
    "email":  "dspsupport@dreamfactory.com",
    "source": "https://github.com/dreamfactorysoftware/dreamfactory",
    "issues": "https://github.com/dreamfactorysoftware/dreamfactory/issues",
    "wiki":   "https://wiki.dreamfactory.com"
  },
  "minimum-stability": "dev",
  "prefer-stable":     true,
  "repositories":      [
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-adldap"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-amqp"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-azure-ad"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-limits"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-logger"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-ibmdb2"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-informix"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-memsql"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-mqtt"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-notification"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-oidc"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-oracledb"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-pubsub"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-salesforce"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-saml"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-soap"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-sqlanywhere"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-sqlsrv"
    },
    {
      "type": "vcs",
      "url":  "https://github.com/dreamfactorysoftware/df-system"
    }
  ],
  "require":           {
    "dreamfactory/df-adldap":          "~0.14.0",
    "dreamfactory/df-admin-app":       "dev-feature/DF-license-3.0#20aaa3eb7ac23bd9041448a705d30129210a1fed",
    "dreamfactory/df-amqp":            "~0.1.0",
    "dreamfactory/df-api-docs-ui":     "dev-3.0-beta#873ae7e55396a1149e626061bca339465670c788",
    "dreamfactory/df-apidoc":          "~0.6.1",
    "dreamfactory/df-aws":             "~0.15.1",
    "dreamfactory/df-azure":           "~0.15.1",
    "dreamfactory/df-azure-ad":        "~0.8.0",
    "dreamfactory/df-cache":           "~0.11.0",
    "dreamfactory/df-cassandra":       "~0.12.2",
    "dreamfactory/df-core":            "~0.14.2",
    "dreamfactory/df-couchbase":       "~0.10.1",
    "dreamfactory/df-couchdb":         "~0.15.1",
    "dreamfactory/df-email":           "~0.8.0",
    "dreamfactory/df-file":            "~0.6.2",
    "dreamfactory/df-filemanager-app": "~0.3.0",
    "dreamfactory/df-firebird":        "~0.7.1",
    "dreamfactory/df-git":             "dev-3.0-beta#c40c898f3c437f5203999de32669a21e5bd13064",
    "dreamfactory/df-graphql":         "~0.1.0",
    "dreamfactory/df-limits":          "dev-feature/update-server#150f96a24db6f047c9bf2d48482d45368d820556",
    "dreamfactory/df-logger":          "~0.11.0",
    "dreamfactory/df-ibmdb2":          "~0.15.1",
    "dreamfactory/df-informix":        "~0.5.1",
    "dreamfactory/df-memsql":          "~0.1.0",
    "dreamfactory/df-mongodb":         "~0.15.1",
    "dreamfactory/df-mqtt":            "~0.4.0",
    "dreamfactory/df-notification":    "~0.7.0",
    "dreamfactory/df-oauth":           "~0.14.0",
    "dreamfactory/df-oidc":            "~0.5.0",
    "dreamfactory/df-oracledb":        "~0.15.1",
    "dreamfactory/df-rackspace":       "~0.14.1",
    "dreamfactory/df-rws":             "~0.14.1",
    "dreamfactory/df-salesforce":      "~0.15.1",
    "dreamfactory/df-saml":            "~0.8.0",
    "dreamfactory/df-script":          "dev-3.0-beta#c2ee23a1a96180fbfa614977711cfe05047f560b",
    "dreamfactory/df-soap":            "~0.14.1",
    "dreamfactory/df-sqldb":           "~0.15.2",
    "dreamfactory/df-sqlanywhere":     "~0.15.1",
    "dreamfactory/df-sqlsrv":          "~0.15.1",
    "dreamfactory/df-system":          "~0.2.0",
    "dreamfactory/df-user":            "dev-feature/update-server#38adf34bdfd2c4c2ef0aa0fae7593de06217611e",
    "predis/predis":                   "~1.0",
    "fideloper/proxy":                 "~3.3",
    "laravel/framework":               "5.5.*",
    "laravel/tinker":                  "~1.0"
  },
  "require-dev":       {
    "barryvdh/laravel-ide-helper": "~2.1",
    "filp/whoops":                 "~2.0",
    "fzaninotto/faker":            "~1.4",
    "laracasts/generators":        "~1.0",
    "laracasts/testdummy":         "~2.0",
    "laravel/homestead":           "6.3.0",
    "mockery/mockery":             "~1.0",
    "phpunit/phpunit":             "~6.0"
  },
  "autoload":          {
    "classmap": [
      "database/seeds",
      "database/factories"
    ],
    "psr-4":    {
      "DreamFactory\\": "app/"
    }
  },
  "autoload-dev":      {
    "psr-4": {
      "Tests\\": "tests/"
    }
  },
  "extra":             {
    "branch-alias":    {
      "dev-develop": "2.12.x-dev"
    },
    "installer-paths": {
      "public/{$name}/": [
        "type:dreamfactory-app"
      ]
    },
    "laravel":         {
      "dont-discover": [
      ]
    }
  },
  "scripts":           {
    "post-autoload-dump": [
      "Illuminate\\Foundation\\ComposerScripts::postAutoloadDump",
      "@php artisan package:discover"
    ]
  },
  "config":            {
    "preferred-install":   "dist",
    "sort-packages":       true,
    "optimize-autoloader": true
  }
}
