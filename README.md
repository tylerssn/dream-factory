## Quick Setup
_Quick setup for development environment._

1. Copy `.env.dist` to `.env` and provide variable values
   - APP_KEY can be generated with `lando composer artisan key:generate` after Step #2
   - DF_LICENSE_KEY can be provided from a project admin
1. Start the local instance: `lando start`
1. Your site should now be available at: https://dreamfactory.monin.tech

## Platform.sh

`platformsh/laravel-bridge` automatically configures this project to connect to the Platform.sh database.

GitLab automatically pushes branch updates to Platform.sh
